/**
 * Redistribution and use of this software and associated documentation
 * ("Software"), with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * 1. Redistributions of source code must retain copyright
 *    statements and notices.  Redistributions must also contain a
 *    copy of this document.
 *
 * 2. Redistributions in binary form must reproduce the
 *    above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. The name "Exolab" must not be used to endorse or promote
 *    products derived from this Software without prior written
 *    permission of Exoffice Technologies.  For written permission,
 *    please contact jima@intalio.com.
 *
 * 4. Products derived from this Software may not be called "Exolab"
 *    nor may "Exolab" appear in their names without prior written
 *    permission of Exoffice Technologies. Exolab is a registered
 *    trademark of Exoffice Technologies.
 *
 * 5. Due credit should be given to the Exolab Project
 *    (http://www.exolab.org/).
 *
 * THIS SOFTWARE IS PROVIDED BY EXOFFICE TECHNOLOGIES AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * EXOFFICE TECHNOLOGIES OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright 2004-2005 (C) Exoffice Technologies Inc. All Rights Reserved.
 *
 * $Id: Receiver.java,v 1.2 2005/11/18 03:28:01 tanderson Exp $
 */
package fr.miage.toulouse.m2.eai.miagebourseproducer;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Programme Client de l'application bourse et permettant l'alimentation de la bourse en titres boursiers via JMS.
 * <p>
 * Utilise une file dénommée zesuperfile et une connection Factory nommée : ConnectionFactory. Ces deux éléments doivent être déployés sur
 * le serveur JMS.
 *
 * @version 1.1, 11 oct. 2019
 * @since 0.1, 3 oct. 2016
 */
public class JMSProducerExample {

    /**
     * Programme principal autonome.
     *
     * @param args Non utilisé.
     */
    public static void main(String[] args) {

        Context context = null;
        ConnectionFactory factory = null;
        Connection connection = null;
        String factoryName = "ConnectionFactory";
        String destName = "zesuperfile";
        Destination dest = null;
        int count = 0;
        Session session = null;
        MessageProducer sender = null;

        /*
         * Alimentation d'une liste de titres
         */
        ArrayList<String> titresaajouter = new ArrayList<>();
        titresaajouter.add("{mnemo : \"AAPL\", nom : \"Apple Corp.\", cours : 1000.0}");
        titresaajouter.add("{mnemo : \"MSFT\", nom : \"Microsoft Corp.\", cours : 50.0}");
        titresaajouter.add("{mnemo : \"GOOGL\", nom : \"Google Corp.\", cours : 666.66}");
        titresaajouter.add("{mnemo : \"UPS\", nom : \"UPS\", cours : 2.0}");
        titresaajouter.add("{mnemo : \"THDS\", nom : \"Ze Only T.\", cours : 500000.0}");

        try {
            // create the JNDI initial context
            context = new InitialContext();

            // look up the ConnectionFactory
            factory = (ConnectionFactory) context.lookup(factoryName);

            // look up the Destination
            dest = (Destination) context.lookup(destName);

            // create the connection
            connection = factory.createConnection();

            // create the session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // create the Producer
            sender = session.createProducer(dest);

            // start the connection, to enable message sending
            connection.start();

            // boucle de production
            for (String text : titresaajouter) {
                TextMessage message = session.createTextMessage();
                message.setText(text);
                sender.send(message);
                System.out.println("Sent: " + message.getText());
            }
        } catch (NamingException ex) {
            Logger.getLogger(JMSProducerExample.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JMSException ex) {
            Logger.getLogger(JMSProducerExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
