/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.core.controllers;

import fr.miage.toulouse.m2.eai.miagebourse.core.entities.BourseBD;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreExistantException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreInconnuException;
import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.util.Collection;
import javax.ejb.Singleton;

/**
 * Singleton Controleur de la Bourse. Permet de conserver 1 seule "BD" bourse.
 * <p>
 * Se focalise sur les opérations techniquesde gestion de la Bourse.
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 0.1, 3 oct. 2016
 */
@Singleton
public class BourseBean implements BourseBeanLocal {

    /**
     * Attribut représentant la BD de la Bourse
     *
     * @see BourseBD
     */
    private BourseBD bdbanque;

    /**
     * Constructeur initialisant une BD vide de la Bourse
     */
    public BourseBean() {
        this.bdbanque = new BourseBD();

    }

    /**
     * Ajoute une nouveau Titre Boursier à la Bourse.
     *
     * @param t Titre Boursier à ajouter
     *
     * @return Titre Boursier ajouté
     *
     * @throws TitreExistantException si le titre boursier est déjà présent en base
     */
    @Override
    public TitreBoursier ajouterTitre(TitreBoursier t) throws TitreExistantException {
        if (this.bdbanque.lestitres.containsKey(t.getMnemo())) {
            throw new TitreExistantException();
        } else {
            this.bdbanque.lestitres.put(t.getMnemo(), t);
            return t;
        }
    }

    /**
     * Récupère les informations d'un Titre Boursier en base.
     *
     * @param mnemo Mnemonique du Titre boursier recherché
     *
     * @return Titre Boursier
     *
     * @throws TitreInconnuException si la Mnemonique du Titre Boursier n'est pas trouvee en base.
     */
    public TitreBoursier getTitre(String mnemo) throws TitreInconnuException {
        if (!this.bdbanque.lestitres.containsKey(mnemo)) {
            throw new TitreInconnuException();
        } else {
            return this.bdbanque.lestitres.get(mnemo);
        }
    }

    /**
     * Recupère la liste des Titres sour la forme d'une collection des mnémoniques.
     *
     * @return liste des Titres. Collection vide si aucun Titre n'est présent en base.
     */
    @Override
    public Collection<String> getListeTitres() {
        return (Collection) this.bdbanque.lestitres.keySet();
    }

    /**
     * Retire (supprime) un Titre Boursier de la base
     *
     * @param mnemo Mnemonique du Titre boursier à retirer
     *
     * @throws TitreInconnuException si la Mnemonique du Titre Boursier n'est pas trouvee en base.
     */
    @Override
    public void retraitTitre(String mnemo) throws TitreInconnuException {
        if (!this.bdbanque.lestitres.containsKey(mnemo)) {
            throw new TitreInconnuException();
        } else {
            this.bdbanque.lestitres.remove(mnemo);
        }
    }

    /**
     * Met à jour le cours d'un titre boursier.
     *
     * @param mnemo Mnemonique du Titre boursier à mettre à jour
     * @param cours Nouveau cours de bourse du titre
     *
     * @throws TitreInconnuException si le Titre Boursier n'est pas présent en base.
     */
    @Override
    public void updateCours(String mnemo, double cours) throws TitreInconnuException {
        if (!this.bdbanque.lestitres.containsKey(mnemo)) {
            throw new TitreInconnuException();
        } else {
            this.bdbanque.lestitres.get(mnemo).setCours(cours);
        }
    }
}
