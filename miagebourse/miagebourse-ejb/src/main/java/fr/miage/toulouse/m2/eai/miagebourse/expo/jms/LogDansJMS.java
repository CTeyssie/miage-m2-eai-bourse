/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.expo.jms;

import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Topic;

/**
 *
 * Service EJB permettant aux autres EJB de faire du log dans un Topic JMS. Le Topic sera nommé zesupertopic.
 *
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 1.1, 11 oct. 2019
 */
@Singleton
public class LogDansJMS implements LogDansJMSLocal {

    /**
     * Nom du Topic recherché.
     */
    @Resource(mappedName = "zesupertopic")
    private Topic zesupertopic;
    /**
     * contexte JMS. Injection auto par serveur d'appli.
     */
    @Inject
    @JMSConnectionFactory("ConnectionFactory")
    private JMSContext context;

    /**
     * Objet Producteur JMS
     */
    //private final JMSProducer producer;
    /**
     * Méthode d'envoi d'un titre Boursier comme log.
     *
     * @param t      Titre Boursier à envoyer.
     * @param niveau Niveau de log à employer. Sera utilisé commme filtre JMS.
     */
    @Override
    public void sendLog(TitreBoursier t, String niveau) {
        try {
            JMSProducer producer = context.createProducer();

            ObjectMessage mess = context.createObjectMessage();
            mess.setJMSType(niveau);
            mess.setObject(t);
            context.createProducer().send(zesupertopic, mess);
            System.out.println(t + " envoyé.");

        } catch (JMSException ex) {
            Logger.getLogger(LogDansJMS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Constructeur. Initialise le producteur JMS.
     */
    public LogDansJMS() {
        //this.producer = context.createProducer();
    }

}
