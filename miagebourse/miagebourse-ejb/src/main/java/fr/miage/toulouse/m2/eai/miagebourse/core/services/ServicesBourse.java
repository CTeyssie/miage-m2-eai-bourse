/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.core.services;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.BasedeTitresVideException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreExistantException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreInconnuException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreIncorrectException;
import fr.miage.toulouse.m2.eai.miagebourse.core.metier.BourseBusiness;
import fr.miage.toulouse.m2.eai.miagebourse.core.metier.BourseBusinessLocal;
import fr.miage.toulouse.m2.eai.miagebourse.expo.jms.LogDansJMSLocal;
import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * Classe regroupant les services pouvant être exposés à l'extérieur du coeur de l'application de la bourse.
 *
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 0.1, 3 oct. 2016
 */
@Stateless
public class ServicesBourse implements ServicesBourseLocal {

    /**
     * EJB de Log dans JMS. Pour l'exemple...
     */
    @EJB
    private LogDansJMSLocal logDansJMS;

    /**
     * EJB métier de la bourse
     */
    @EJB
    private BourseBusinessLocal bourseBusiness;
    /**
     * Convertisseur Objet JSON et inversement)
     */
    private Gson gson;

    /**
     * Constructeur par défaut de l'exposition
     */
    public ServicesBourse() {
        this.gson = new Gson();
    }

    /**
     * Ajoute un titre boursier à la base de titres à partie d'un JSON.
     *
     * @param t Titre Boursier au format JSON complet à ajouter. Ce titre n'a pas besoin d'être complet. A minima, mnemonique, nom et cours.
     *
     * @return JSON du Titre boursier ajouté en base
     *
     * @throws TitreExistantException  Levée si le titre existe déjà en base
     * @throws TitreIncorrectException Levée si le titre est mal formatté. Ex: Pas de mnemonique.
     */
    @Override
    public String ajouterTitre(String t) throws TitreExistantException, TitreIncorrectException {
        try {
            System.out.println(t);
            TitreBoursier titre = this.gson.fromJson(t, TitreBoursier.class);
            // titrevalide permet de positionner correctement la date de prise en compte de la cotation
            TitreBoursier titrevalide = new TitreBoursier(titre.getMnemo(), titre.getNom(), titre.getCours());

            this.logDansJMS.sendLog(titre, "AJOUT");
            return this.gson.toJson(this.bourseBusiness.ajouterTitre(titrevalide));
        } catch (JsonSyntaxException e) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, e);
            throw new TitreIncorrectException();
        }
    }

    /**
     * Ajoute un titre boursier à la base de titres à partir des informations séparées.
     *
     * @param mnemo mnemonique du titre à ajouter
     * @param nom   nom du titre à ajouter
     * @param cours cours du titre
     *
     * @return JSON du Titre boursier ajouté en base
     *
     * @throws TitreExistantException  Levée si le titre existe déjà en base
     * @throws TitreIncorrectException Levée si le titre est mal formatté. Ex: Pas de mnemonique.
     */
    @Override
    public String ajouterTitre(String mnemo, String nom, String cours) throws TitreExistantException, TitreIncorrectException {
        if (mnemo.isEmpty() || cours.isEmpty()) {
            throw new TitreIncorrectException();
        }
        try {
            Double lecours = Double.parseDouble(cours);
            TitreBoursier t = new TitreBoursier(mnemo, nom, lecours);
            this.logDansJMS.sendLog(t, "AJOUT");
            return this.gson.toJson(this.bourseBusiness.ajouterTitre(t));
        } catch (NullPointerException | NumberFormatException e) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, e);
            throw new TitreIncorrectException();
        }
    }

    /**
     * Supprime un titre boursier de la base
     *
     * @param mnemo mnemonique du titre à supprimer
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public void retraitTitre(String mnemo) throws TitreInconnuException {
        TitreBoursier t = this.bourseBusiness.getTitre(mnemo);
        this.logDansJMS.sendLog(t, "RETRAIT");
        this.bourseBusiness.retraitTitre(t);
    }

    /**
     * Retourne un titre boursier en base à partir de sa mnémonique
     *
     * @param mnemo Mnémonique du titre à rechercher
     *
     * @return JSON du Titre Boursier recherché
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public String getTitre(String mnemo) throws TitreInconnuException {
        return this.gson.toJson(this.bourseBusiness.getTitre(mnemo));
    }

    /**
     * Retourne la liste des mnenomique des titres en base au format JSON.
     *
     * @return Collection des mnemoniques
     *
     * @throws BasedeTitresVideException Levée si la liste de titres est vide
     */
    @Override
    public String getListeTitres() throws BasedeTitresVideException {
        Collection<String> listetitres = this.bourseBusiness.getListeTitres();
        if (listetitres.isEmpty()) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, "Aucun titre en Base");

            throw new BasedeTitresVideException();
        }
        return this.gson.toJson(listetitres);
    }

    /**
     * Met à jour le cours d'un Titre Boursier en base à partir d'un objet titre JSON
     *
     * @param t Titre boursier au format JSON à mettre à jour avec les valeurs de cours actualisées.
     *
     * @return JSON du Titre Boursier mis à jour
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public String majTitre(String t) throws TitreInconnuException {
        try {
            TitreBoursier titre = this.gson.fromJson(t, TitreBoursier.class);
            this.bourseBusiness.majTitre(titre);
            return this.gson.toJson(this.bourseBusiness.getTitre(titre.getMnemo()));
        } catch (TitreInconnuException ex) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, ex);
            throw new TitreInconnuException();
        }
    }

    /**
     * Met à jour le cours d'un Titre Boursier en base à partir d'une mnemonique et d'un nouveau cours de bourse.
     *
     * @param id    mnemonique du Titre boursier à mettre à jour
     * @param cours Valeur actualisée du cours
     *
     * @return JSON du Titre Boursier mis à jour
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public String majTitre(String id, String cours) throws TitreInconnuException {
        try {
            TitreBoursier titre = this.bourseBusiness.getTitre(id);
            this.bourseBusiness.majTitre(titre, Double.parseDouble(cours));
            return this.gson.toJson(this.bourseBusiness.getTitre(id));
        } catch (TitreInconnuException ex) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, ex);
            throw new TitreInconnuException();
        }
    }

}
