/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.expo.jms;

import com.google.gson.Gson;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreExistantException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreIncorrectException;
import fr.miage.toulouse.m2.eai.miagebourse.core.metier.BourseBusinessLocal;
import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * Message Driven Bean permettant la réception de nouveaux titres à ajouter dnas la base.
 *
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 1.1, 11 oct. 2019
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "zesuperfile")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class ReceptionNouveauTitre implements MessageListener {

    /**
     * EJB de Log dans Topic JMS.
     */
    @EJB
    private LogDansJMSLocal logDansJMS;

    /**
     * EJB de traitement du métier
     */
    @EJB
    private BourseBusinessLocal bourseBusiness;

    /**
     * Convertisseur JSON
     */
    private Gson gson;

    /**
     * Constructeur du MDB
     */
    public ReceptionNouveauTitre() {
        this.gson = new Gson();
    }

    /**
     * Méthode invoquée lors de la réception d'un message dans la file JMS d'ajout de nouveaux titres.
     *
     * @param message Message JMS reçu contenant le titrre à ajouter dans la base
     */
    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                String json = ((TextMessage) message).getText();

                TitreBoursier titre = this.gson.fromJson(json, TitreBoursier.class);
                System.out.println("Received: " + titre.getMnemo());
                try {
                    this.bourseBusiness.ajouterTitre(titre);
                    this.logDansJMS.sendLog(titre, "AJOUT");
                } catch (TitreExistantException ex) {
                    Logger.getLogger(ReceptionNouveauTitre.class.getName()).log(Level.SEVERE, null, ex);
                } catch (TitreIncorrectException ex) {
                    Logger.getLogger(ReceptionNouveauTitre.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (JMSException ex) {
                Logger.getLogger(ReceptionNouveauTitre.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (message != null) {
            System.out.println("Received non TitreBoursier message");
        }
    }

}
