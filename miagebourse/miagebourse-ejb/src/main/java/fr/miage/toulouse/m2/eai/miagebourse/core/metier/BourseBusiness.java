/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.core.metier;

import fr.miage.toulouse.m2.eai.miagebourse.core.controllers.BourseBeanLocal;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreExistantException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreInconnuException;
import fr.miage.toulouse.m2.eai.miagebourse.core.exceptions.TitreIncorrectException;
import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * Classe regroupant le métier de la bourse, principalement les méthodes de CRUD.
 *
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 0.1, 11 oct. 2016
 */
@Stateless
public class BourseBusiness implements BourseBusinessLocal {

    /**
     * EJB permettant la manipulation technique de la base
     */
    @EJB
    private BourseBeanLocal bourseBean;

    /**
     * Constrcuteur par défaut du métier Bourse
     */
    public BourseBusiness() {
    }

    /**
     * Ajoute un titre à la base de titres.
     *
     * @param t Titre Boursier complet à ajouter. Ce titre doit être complet. Tous les attributs du titre doivent être fournis. Aucune
     *          vérification de complétude n'est faite ici.
     *
     * @return Titre boursier ajouté en base
     *
     * @throws TitreExistantException  Levée si le titre existe déjà en base
     * @throws TitreIncorrectException Levée si le titre est mal formatté. Ex: Pas de mnemonique.
     */
    @Override
    public TitreBoursier ajouterTitre(TitreBoursier t) throws TitreExistantException, TitreIncorrectException {
        // vérif mnemonique Titre
        if (t.getMnemo() == null) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, "TitreIncorrectException");
            throw new TitreIncorrectException();
        }
        if (t.getMnemo().isEmpty()) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, "TitreIncorrectException");
            throw new TitreIncorrectException();
        }

        // ajout
        return this.bourseBean.ajouterTitre(t);
    }

    /**
     * Supprime un titre de la base
     *
     * @param t titre Boursier à supprimer
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public void retraitTitre(TitreBoursier t) throws TitreInconnuException {
        this.bourseBean.retraitTitre(t.getMnemo());
    }

    /**
     * Retourne un titre en base à partir de sa mnémonique
     *
     * @param mnemo Mnémonique du titre à rechercher
     *
     * @return Titre Boursier
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public TitreBoursier getTitre(String mnemo) throws TitreInconnuException {
        return this.bourseBean.getTitre(mnemo);
    }

    /**
     * Retourne la liste des mnenomique des titres en base
     *
     * @return Collection des mnemoniques
     */
    @Override
    public Collection<String> getListeTitres() {
        return this.bourseBean.getListeTitres();
    }

    /**
     * Met à jour le cours d'un Titre Boursier en base à partir d'un objet Titre pré-rempli
     *
     * @param titre Titre boursier à mettre à jour avec les valeurs de cours actualisées.
     *
     * @return Titre Boursier mis à jour
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public TitreBoursier majTitre(TitreBoursier titre) throws TitreInconnuException {
        try {
            this.bourseBean.updateCours(titre.getMnemo(), titre.getCours());
            return this.bourseBean.getTitre(titre.getMnemo());
        } catch (TitreInconnuException ex) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, ex);
            throw new TitreInconnuException();
        }
    }

    /**
     * Met à jour le cours d'un Titre Boursier en base à partir d'un objet Titre et d'un nouveau cours de bourse.
     *
     * @param titre Titre boursier à mettre à jour
     * @param cours Valeur actualisée du cours
     *
     * @return Titre Boursier mis à jour
     *
     * @throws TitreInconnuException Levée si le titre est absent de la base
     */
    @Override
    public TitreBoursier majTitre(TitreBoursier titre, Double cours) throws TitreInconnuException {
        try {
            this.bourseBean.updateCours(titre.getMnemo(), cours);
            return this.bourseBean.getTitre(titre.getMnemo());
        } catch (TitreInconnuException ex) {
            Logger.getLogger(BourseBusiness.class.getName()).log(Level.SEVERE, null, ex);
            throw new TitreInconnuException();
        }
    }

}
