/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.expo.jms;

import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import javax.ejb.Local;

/**
 *
 * Service EJB permettant aux autres EJB de faire du log dans un Topic JMS. Le Topic sera nommé zesupertopic.
 * <p>
 * Dans cette version, SEULS les AJOUTS et RETRAITS appellés sur les services sont logges.
 *
 * @see fr.miage.toulouse.m2.eai.miagebourse.core.services
 *
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 1.1, 11 oct. 2019
 */
@Local
public interface LogDansJMSLocal {

    public void sendLog(TitreBoursier t, String niveau);
}
