/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.core.entities;

import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.util.HashMap;

/**
 *
 * Base de données de la Bourse sour la forme d'une table de Hash.
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 1.1, 11 oct. 2019
 */
// miagebourse-ejb
// fr.miage.toulouse.m2.eai.miagebourse.core.entities
// BourseBD.java
public class BourseBD {

    /**
     * Objet contenant l'ensemble des titres boursiers connus dont la clé est la mnémonique du Titre boursier.
     */
    public HashMap<String, TitreBoursier> lestitres;

    /**
     * constructeur initialisant une nouvelle base vide.
     */
    public BourseBD() {
        this.lestitres = new HashMap<>();
    }

}
