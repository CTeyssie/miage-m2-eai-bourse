/**
 * Copyright © ${project.inceptionYear} MIAGE de Toulouse (cedric.teyssie@miage.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.miage.toulouse.m2.eai.miagebourse.shared;

import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * Entité représentant un titre Boursier.
 *
 * @author Cédric Teyssié  <cedric.teyssie@irit.fr>, IRIT-SIERA, Université Paul Sabatier
 * @version 1.1, 11 oct. 2019
 * @since 0.1, 3 oct. 2016
 */
// BourseEJB
// entities
// TitreBoursier.java
public class TitreBoursier implements Serializable {

    /**
     * Mnemonique du Titre. Ex : AAPL
     */
    private String mnemo;
    /**
     * Nom titre. Ex : Apple Inc.
     */
    private String nom;
    /**
     * Cours de bourse en devise locale. Ex : 25.12
     */
    private double cours;
    /**
     * Date de la dernière mise à jour du Titre sous la forme d'un timestamp.
     */
    private long datecours;
    /**
     * Variation du cours de bourse en % depuis la dernière mise à jour. 100% indique une abszence de variation.
     */
    private double variation;

    /**
     *
     * @param mnemo
     * @param nom
     * @param cours
     */
    public TitreBoursier(String mnemo, String nom, double cours) {
        this.mnemo = mnemo;
        this.nom = nom;
        this.cours = cours;
        this.datecours = Calendar.getInstance().getTimeInMillis();
        this.variation = 0;
    }

    /**
     * Getter de la mnemonique d'un titre.
     *
     * @return mnemonique d'un titre
     */
    public String getMnemo() {
        return mnemo;
    }

    /**
     * Setter de la mnemonique d'un titre.
     *
     * @param mnemo mnemonique à affecter pour le titre
     */
    public void setMnemo(String mnemo) {
        this.mnemo = mnemo;
    }

    /**
     * Getter du nom d'un titre.
     *
     * @return Nom du titre
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter du nom d'un titre.
     *
     * @param nom Nom à affecter pour le titre
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter du cours d'un titre.
     *
     * @return Cours du titre
     */
    public double getCours() {
        return cours;
    }

    /**
     * Setter du cours d'un titre.
     *
     * @param cours cours à affecter pour le titre
     */
    public void setCours(double cours) {
        this.setDatecours(Calendar.getInstance().getTimeInMillis());
        this.variation = (cours / this.cours) * 100;
        this.cours = cours;
    }

    /**
     * Getter de la date du cours d'un titre.
     *
     * @return Date du cours du titre
     */
    public long getDatecours() {
        return datecours;
    }

    /**
     * Setter de la date d'un cours d'un titre.
     *
     * @param datecours date du cours à affecter pour le titre
     */
    public void setDatecours(long datecours) {
        this.datecours = datecours;
    }

    /**
     * Getter de la variation du cours d'un titre.
     *
     * @return Variation du cours du titre
     */
    public double getVariation() {
        return variation;
    }

    /**
     * Setter de la variation du coursd'un titre.
     *
     * @param variation variation du cours à affecter pour le titre
     */
    public void setVariation(double variation) {
        this.variation = variation;
    }

}
