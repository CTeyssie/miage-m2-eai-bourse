/**
 * Redistribution and use of this software and associated documentation
 * ("Software"), with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * 1. Redistributions of source code must retain copyright
 *    statements and notices.  Redistributions must also contain a
 *    copy of this document.
 *
 * 2. Redistributions in binary form must reproduce the
 *    above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. The name "Exolab" must not be used to endorse or promote
 *    products derived from this Software without prior written
 *    permission of Exoffice Technologies.  For written permission,
 *    please contact jima@intalio.com.
 *
 * 4. Products derived from this Software may not be called "Exolab"
 *    nor may "Exolab" appear in their names without prior written
 *    permission of Exoffice Technologies. Exolab is a registered
 *    trademark of Exoffice Technologies.
 *
 * 5. Due credit should be given to the Exolab Project
 *    (http://www.exolab.org/).
 *
 * THIS SOFTWARE IS PROVIDED BY EXOFFICE TECHNOLOGIES AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * EXOFFICE TECHNOLOGIES OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright 2004-2005 (C) Exoffice Technologies Inc. All Rights Reserved.
 *
 * $Id: Receiver.java,v 1.2 2005/11/18 03:28:01 tanderson Exp $
 */
package fr.miage.toulouse.m2.eai.miagebourseconsumer;

import fr.miage.toulouse.m2.eai.miagebourse.shared.TitreBoursier;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Programme Client de l'application bourse et permettant de logguer la bourse via JMS.
 * <p>
 * Utilise un topic pour l'exemple.
 *
 * @version 1.1, 11 oct. 2019
 * @since 0.1, 3 oct. 2016
 */
public class JMSConsumerExample {

    /**
     * Programme principal autonome.
     *
     * @param args Non utilisé.
     */
    public static void main(String[] args) {

        Context context = null;
        ConnectionFactory factory = null;
        Connection connection = null;
        String factoryName = "ConnectionFactory";
        String destName = "zesupertopic";
        Topic dest = null;

        Session session = null;

        /*
         * Configure le niveau de log du serveur de bourse. A choisir parmi : AJOUT : log tous les titres Boursiers ajoutés. RETRAIT : log
         * tous les titres boursiers supprimés
         */
        String niveauLOG = "AJOUT";
        /*
         * Nombre d'evenements à logguer.
         */
        int count = 10;

        try {
            // create the JNDI initial context
            context = new InitialContext();

            // look up the ConnectionFactory
            factory = (ConnectionFactory) context.lookup(factoryName);

            // look up the Destination
            dest = (Topic) context.lookup(destName);

            // create the connection
            connection = factory.createConnection();

            // Génération ID unique Client. D'autres moyens sont possibles. Certains serveurs exigent des formats particuliers.
            connection.setClientID(InetAddress.getLocalHost().getHostName());

            // create the session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // create the receiver
            // Les Messages JMS sont filtrés sur le niveau de log demandé.
            TopicSubscriber subscriber = session.createDurableSubscriber(dest, "OldElPaso", "JMSType='" + niveauLOG + "'", false);

            connection.start();

            for (int i = 0; i < count; i++) {
                Message message = subscriber.receive();
                if (message instanceof ObjectMessage) {
                    ObjectMessage om = (ObjectMessage) message;
                    TitreBoursier titre = (TitreBoursier) om.getObject();
                    System.out.println("Received: " + titre.getMnemo());
                } else if (message != null) {
                    System.out.println("Received non object message");
                } else {
                    System.out.println("???");
                }
            }
        } catch (NamingException ex) {
            Logger.getLogger(JMSConsumerExample.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JMSException ex) {
            Logger.getLogger(JMSConsumerExample.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(JMSConsumerExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
